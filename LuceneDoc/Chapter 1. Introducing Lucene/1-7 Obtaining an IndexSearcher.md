## Obtaining an IndexSearcher

Building index is necessary, an index make your text searchable.

Lucene leverages an `inverted index`. Lucene can now locate documents quickly by stepping into the term Lucene in the index, and returning all the related documents by their DocIds.

An index can in a form of file or in-memory, it depends on `IndexWriter` when you are creating an index.

```java
Directory directory = getDirectory();

// a static method that opens an index to read
IndexReader indexReader = DirectoryReader.open(directory);
IndexSearcher indexSearcher = new IndexSearcher(indexReader);

```