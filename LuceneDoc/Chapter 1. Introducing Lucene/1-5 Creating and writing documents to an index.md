

## Creating and writing documents to an index

```java

Analyzer analyzer = new WhitespaceAnalyzer();
Directory directory = new RAMDirectory();
IndexWriterConfig config = new IndexWriterConfig(analyzer);
IndexWriter indexWriter = new IndexWriter(directory, config);

Document doc = new Document();
String text = "Lucene is an Information Retrieval library written in java";
doc.add(
    new TextField("fieldname", text, Field.Store.Yes)
);

indexWriter.addDocument(doc);
indexWritter.close();

```