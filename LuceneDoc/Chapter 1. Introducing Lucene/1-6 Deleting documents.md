## Deleting documents

IndexWriter provides the interface to delete documents from an index.
* deleteDocuments(Term)
* deleteDocuments(Term... terms)
* deleteDocuments(Query)
* deleteDocuments(Query... queries)
* deleteAll()

```java

indexWriter.deleteDocuments(
    new Term("id","1")
);
indexWriter.close();
```

Note that this is a match to a Field called `id`; it's not the same as `DocId`.

* actually, the documents are initially marked as deleted on disk, to free the memory, you still need to wait. We will see the underlying process in detail in due course.