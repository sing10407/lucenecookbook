## Performing a search

Now we will leverage IndexSearcher to perform a search.

A search result is sorted/ranked by score, and score is calculated by number of field hit.



```java
StandardAnalyzer analyzer = new StandardAnalyzer();
Directory index = new RAMDirectory();
IndexWriterConfig config = new IndexWriterConfig(analyzer);

// write documents to index
IndexWriter indexWritter = new IndexWriter(index, config);

// create a document object
Document doc = new Document();
String text = "Lucene is an Information Retrieval library written in java";
doc.add(new TextField("Content", text, Field.Store.YES));

indexWritter.addDocument(doc);
indexWritter.close();

// Create Query object, use "Lucene" as keyword and search on field "Content"
Query query = new QueryParser("Content", analyzer).parse("Lucene");

int hitsPerPage = 10;

// get a searcher
IndexReader reader = DirectoryReader.open(index);
IndexSearcher searcher = new IndexSearcher(reader);

// perform search (search on index created before)
TopDocs docs = searcher.search(query, hitsPerPage);
ScoreDoc[] hits = docs.scoreDocs;

// display results
System.out.println("Found " + hits.length + " hits.");
for (int i = 0; i < hits.length; ++i) {
    int docId = hits[i].doc; //get doc id
    Document d = searcher.doc(docId);
    System.out.println(d.get("Content"));
}

```

In `TopDocs`, we only get back an array of `ranked pointers`. It's called ranked pointers because they are not actual documents, but a list of references(DocId). By defalut, results are scored by the scoring mechanism.