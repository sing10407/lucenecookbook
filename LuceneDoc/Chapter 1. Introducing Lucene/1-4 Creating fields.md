## Creating fields


```java

Document doc = new Document();
String text = "Lucene is an Information Retrieval library written in java";
doc.add(
    new TextField("fieldname", text, Field.Store.Yes)
);

```

A Lucene field has three attributes:
* Name
* Type
* Value

A Lucene field can hold the following:
* String
* Reader or preanalyzed TokenStream
* Binary(byte[])