
## basic sample


```java
// for parse input text, and tokenize text into word tokens.
Analyzer analyzer= new WhitespaceAnalyzer();

// create in-memory dir
// RAMDirectory was Deprecated 
// https://lucene.apache.org/core/7_5_0/core/org/apache/lucene/store/RAMDirectory.html
Directory directory = new RAMDirectory();  

IndexWriterConfig config = new IndexWriterConfig(analyzer);

// then create index writer
// Lucene knows where to persist indexing information
// and what treatment to apply to the documents before they are indexed
IndexWriter indexWriter = new IndexWriter(directory, config);
```