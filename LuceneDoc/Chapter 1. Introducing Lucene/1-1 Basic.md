

## Basic process flow for a search application

```mermaid
graph TD;
    data[prepared data] --build data into an index--> Index
    Search-->Index


```



* In the first stage, you need to prepare your data that want to be searched. Maybe from a database or documents.

* The second stage, the data will be transformed into an search engine index and stored.

* The last stage, any query will query the index and return result.

---
## Basic Indexing concept

Lucene will give each document an id, like:
```
doc id 1:  Lucene
doc id 2:  Lucene and Solr
doc id 3:  Solr extends Lucene
```
and segment each document by `Analyzer`. In english, it's easy to segment by space, like `This is a pen` can split into `this`, `is`, `a`, `pen`, but it's hard to split chinese or japanese sentences like `台灣小吃很有名`.

And then Lucene will tokenize the phrases into keywords and build into an `inverted index`.

|word|doc id set|
|-|-|
|and|2|
|extends|3|
|lucene|1,2,3|
|solr|2,3|

and when query this index with keyword `lucene`, we will find result are document id 1, 2, 3.


---
### Discussion

But what should we do if we add more feature?

* search by multi-keywords
* synonyms
* convert text between different letter cases
* data with multiple field 
    * give each field different weight
    * search certain field
* group result
* ranking by field, weight
* advanced search
    * price range
    * search by category

---


