
# Analyzer

* the `org.apache.lucene.analysis.core` module contains all most commonly-used analyzers.
* for language specific analysis, you can refer to the `org.apache.lucene.analysis` {language code} packages.

## initial

```java

// for parse input text, and tokenize text into word tokens.
Analyzer analyzer= new WhitespaceAnalyzer();
```

```mermaid
graph TD;
    cf[character filter]-->tokenizer
    tokenizer-->tf[token filter]
```


### An analyzer is a wrapper of three major components:
* **character filter**
    * preprocess text
        * striping out HTML muckups
        * removing user-defined patterns
        * removing special character
* **tokenizer**
    * split up text into tokens
* **token filter**
    * post tokenization filtering
    * stemming
    * stop word filtering
    * text normalization
    * synonym expansion

The output of these analysis processes is `TokenStream`  where the indexing process can consume and produce an index.

---

Built-in analyzer:

* StopAnalyzer
    * lowercases text
    * tokenizes non-letter charecters
    * removes stop words
* SimpleAnalyzer
    * similar with `StopAnalyzer`
* StandardAnalyzer
    * consists of
        * StandardTokenizer
            * grammer-based tokenization
            * applicable for most European languages
        * StandardFilter
        * LowerCaseFilter
        * StopWordFilter
    * 
* SnowballAnalyzer
    * StandardTokenizer PLUS stemming.
        * stemming is a technique to reduce words to their word steam or root form.
            * find matches of words with the same meaning, but in different forms such as plural and singular forms.