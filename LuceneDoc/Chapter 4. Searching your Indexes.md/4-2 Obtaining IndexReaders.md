## Obtaining IndexReaders


Lucene provides the `IndexReader` class to access a point-in-time(snapshot) index. It allows you to read index when index is being written, and the uncommitted data would not be searched. This architecture allows you to switch indexs seamlessly by opening a new `IndexReader`.

`DirectoryReader` is a subclass of `IndexReader`, it allows access a directory that containing an index, and return an `IndexReader`, and use `openIfChanged` would reuse the existing `DirectoryReader` for faster re-opening of an index.


```java

// open a directory
Directory directory = FSDirectory.open( new File("/data/index").toPath());
// set up a DirectoryReader
DirectoryReader directoryReader = DirectoryReader.open(directory);

// open another DirectoryReader by calling openIfChanged
DirectoryReader newDirectoryReader = DirectoryReader.openIfChanged(directoryReader);
// assign newDirectoryReader
if (newDirectoryReader != null) {
    IndexSearcher indexSearcher = new IndexSearcher(newDirectoryReader);
    // close the old DirectoryReader
    directoryReader.close();
}
```