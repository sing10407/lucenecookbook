
# todo

how to use

1. SimpleCollector
2. FieldComparator

Besides building your own `SimpleCollector` to customize how the results are stored and sorted, another way to customize results sorting is to implement your own `FieldComparator` for `SortField`.Note that this customization only deals with the sorting aspect of the result set; it does not give you the flexibility to filter out results, as you would have with Collector.


---

