
## Introduction


Basic process in `Analyzer`：

```mermaid
graph TD;
    text-->cf[character filter]
    subgraph Analyzer
        cf--stripping out HTML tas ...etc-->tokenizer[tokenizer]
        tokenizer--breaks up text-->tokenfilter[token filter]
        tokenfilter--stemming or synonym expansion...etc-->tokenlist[token list]
    end
    tokenlist--building index-->index
```

We can remind be mentioned in Chapter 1. ：

An analyzer is a wrapper of three major components:
* **character filter**
    * preprocess text
        * striping out HTML muckups
        * removing user-defined patterns
        * removing special character
* **tokenizer**
    * split up text into tokens
* **token filter**
    * post tokenization filtering
    * stemming
    * stop word filtering
    * text normalization
    * synonym expansion

Analyzer is leveraged in `tokenizing` and `cleansing` data.

A `term` as a fundamental unit of data in Lucene index. It associates with a Document and itself has two attributes-- `field` and `value.`

`Tokenization` is a process that breaks up text at word boundaries defined by a specific tokenizer. After tokenization, filtering kicks in to massage data before outputting to IndexWriter for indexing.

Some features in Analyzer
- **Stopword filtering：**
    - `stopword removal`, words like a, and, the, on, of...etc. It convey no meaning, so we don't need to build a index contains them.
- **Text normalization：**
    - change text to certain standard format, such as lowercasing and removal of special characters such as grave accent.
- **Stemming：**
    - Stemming is a reduction of words to their most basic (root) form, and this process is language-specific. Such as run, runs, running, ran...etc.
        - Stemmer provides by Lucene:
        - `Snowball`
        - `PorterStem`
        - `KStem`
- **Synonym Expansion：**
    - this technique expands a word into additional similar-meaning words for matching, for example, beautiful and pretty, sad and unhappy.
