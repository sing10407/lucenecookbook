## PerFieldAnalyzerWrapper

Documents usually have a lots of fields need to be indexed and searched. Sometimes you need different analyzers to split text. For Example, a product model `GG-5566`, you want to keep the token as `GG-5566`, instead of `GG` and `5566`. So you need a `PerFieldAnalyzerWrapper` to assign certain Analyzer to a field.

```mermaid
graph TD;
    subgraph PerFieldAnalyzerWrapper
        field1-- use WhitespaceAnalyzer---doc
        field2--use StandardAnalyzer---doc
    end
```

### Usage
```java
// define a map of analyzer and field
Map<String, Analyzer> analyzerPerField = new HashMap<String,Analyzer>();
analyzerPerField.put("myfield", new WhitespaceAnalyzer()); // add a field-analyzer pair

PerFieldAnalyzerWrapper defanalyzer = new PerFieldAnalyzerWrapper(new StandardAnalyzer(), analyzerPerField);

...

TokenStream ts = defanalyzer.tokenStream("myfield", new StringReader("lucene.apache.org AB-978"));
```


### Complete Sample Code

```java
// define a map of analyzer and field
Map<String, Analyzer> analyzerPerField = new HashMap<String,Analyzer>();
analyzerPerField.put("myfield", new WhitespaceAnalyzer()); // add a field-analyzer pair

PerFieldAnalyzerWrapper defanalyzer = new PerFieldAnalyzerWrapper(new StandardAnalyzer(), analyzerPerField);

TokenStream ts = null;
OffsetAttribute offsetAtt = null;
CharTermAttribute charAtt = null;
try {
    ts = defanalyzer.tokenStream("myfield", new StringReader("lucene.apache.org AB-978"));
    offsetAtt = ts.addAttribute(OffsetAttribute.class);
    charAtt = ts.addAttribute(CharTermAttribute.class);
    ts.reset();
    System.out.println("== Processing field'myfield' using WhitespaceAnalyzer (per field) ==");
    while (ts.incrementToken()) {
        System.out.println(charAtt.toString());
        System.out.println("token start offset: " + offsetAtt.startOffset());
        System.out.println(" token end offset: " + offsetAtt.endOffset());
    }
    ts.end();
    ts = defanalyzer.tokenStream("content", new StringReader("lucene.apache.org AB-978"));
    offsetAtt = ts.addAttribute(OffsetAttribute.class);
    charAtt = ts.addAttribute(CharTermAttribute.class);
    ts.reset();
    System.out.println("== Processing field 'content' using StandardAnalyzer ==");
    while (ts.incrementToken()) {
        System.out.println(charAtt.toString());
        System.out.println("token start offset: " + offsetAtt.startOffset());
        System.out.println(" token end offset: " + offsetAtt.endOffset());
    }
    ts.end();
}
catch (IOException e) {
    e.printStackTrace();
} finally {
    ts.close();
}
```

### result
```
== Processing field'myfield' using WhitespaceAnalyzer (per field) ==
lucene.apache.org
token start offset: 0
 token end offset: 17
AB-978
token start offset: 18
 token end offset: 24
== Processing field 'content' using StandardAnalyzer ==
lucene.apache.org
token start offset: 0
 token end offset: 17
ab
token start offset: 18
 token end offset: 20
978
token start offset: 21
 token end offset: 24
```