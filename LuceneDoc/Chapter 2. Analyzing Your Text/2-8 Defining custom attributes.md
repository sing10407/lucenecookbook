### Defining custom attributes

We will create a `GenderAttribute` interface extends from `Attribute`, and then create `GenderAttributeImpl` extend a class from `AttributeImpl` and implement `GenderAttribute`.

Part of the requirement of creating a custom Attribute class is the creation of an interface that's based on attribute and an implementation that's based on AttributeImpl. **The names of the classes are important because Lucene looks for the Impl suffix to locate the implementation class of an Attribute interface.**


```mermaid
graph LR;
    GenderAttribute--extends-->Attribute[Attribute Interface]
    GenderAttributeImpl-.impl.->GenderAttribute
    GenderAttributeImpl--extends-->AttributeImpl
```

```java
public interface GenderAttribute extends Attribute {
    public static enum Gender {
        Male,
        Female,
        Undefined
    };

    public void setGender(Gender gender);
    
    public Gender getGender();
}
```

**The names of the classes are important because Lucene looks for the `Impl` suffix to locate the implementation class of an Attribute interface.**

```java
public class GenderAttributeImpl extends AttributeImpl implements GenderAttribute {
    private Gender gender = Gender.Undefined;
    
    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Gender getGender() {
        return gender;
    }

    @Override
    public void clear() {
        gender = Gender.Undefined;
    }

    @Override
    public void reflectWith(AttributeReflector attributeReflector) {
    }

    @Override
    public void copyTo(AttributeImpl target) {
        ((GenderAttribute) target).setGender(gender);
    }
}
```

---

So we can create a `GenderFilter` to see how `GenderAttribute` works.

```mermaid
graph TD;
    GenderFilter---GenderAttribute
    GenderFilter--extends-->TokenFilter
```

```java
public class GenderFilter extends TokenFilter {
    GenderAttribute genderAtt = addAttribute(GenderAttribute.class);
    CharTermAttribute charTermAtt = addAttribute(CharTermAttribute.class);

    protected GenderFilter(TokenStream input) {
        super(input);
    }
    public boolean incrementToken() throws IOException {
        if (!input.incrementToken()) {
            return false;
        }
        genderAtt.setGender(determineGender(charTermAtt.toString()));
        return true;
    }
    protected GenderAttribute.Gender determineGender(String term) {
        if (term.equalsIgnoreCase("mr") || term.equalsIgnoreCase("mister")) {
            return GenderAttribute.Gender.Male; }
        else if (term.equalsIgnoreCase("mrs") || term.equalsIgnoreCase("misters")) {
            return GenderAttribute.Gender.Female;
        }
        return GenderAttribute.Gender.Undefined;
    }
}
```


```java
public class TestAttributeFilters {
    public static void main(String[] argv) throws IOException {
        StringReader reader = new StringReader("Dr kerker and Mrs GG are couple.");
        StandardAnalyzer standardAnalyzer = new StandardAnalyzer();
        TokenStream ts = null;

        try {
            ts = standardAnalyzer.tokenStream("field", reader);
            // use new gender token filter
            ts = new GenderFilter(ts);
            // get attribute from token stream
            CharTermAttribute termAtt = ts.getAttribute(CharTermAttribute.class);
            GenderAttribute genderAtt = ts.getAttribute(GenderAttribute.class);

            ts.reset();
            while (ts.incrementToken()) {
                System.out.println("[" + termAtt.toString() +" Gender："+genderAtt.getGender()+"]");
            }
            ts.end();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            ts.close();
            standardAnalyzer.close();
        }

    }
}

```


### output

```
[dr Gender：Undefined]
[kerker Gender：Undefined]
[and Gender：Undefined]
[mrs Gender：Female]
[gg Gender：Undefined]
[are Gender：Undefined]
[couple Gender：Undefined]
```