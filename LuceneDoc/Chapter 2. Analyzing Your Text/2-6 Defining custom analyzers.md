## 2-6 Defining custom analyzers

You customize an analyzer when the built-in analyzers do not meets all requirements.

We can customize an analyzer by extending from the `Analyzer` and implement the `createComponents()`.

```java
public class CourtesyTitleAnalyzer extends Analyzer {
    @Override
    protected TokenStreamComponents createComponents(String fieldName) {
        Tokenizer letterTokenizer = new LetterTokenizer();
        TokenStream filter = new CourtesyTitleFilter(letterTokenizer);
        return new TokenStreamComponents(letterTokenizer, filter);
    }
}
```

### Complete sample code:

```java
public class CourtesyTitleAnalyzer extends Analyzer {
    public static void main(String[] argv) throws IOException {
        StringReader reader = new StringReader("dr kerker and mrs GG are couple.");
        CourtesyTitleAnalyzer courtesyTitleAnalyzer = new CourtesyTitleAnalyzer();
        TokenStream ts = null;

        try {
            ts = courtesyTitleAnalyzer.tokenStream("field", reader);
            CharTermAttribute termAtt = ts.addAttribute(CharTermAttribute.class);

            ts.reset();
            while (ts.incrementToken()) {
                System.out.println("[" + termAtt.toString() + "]");
            }
            ts.end();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            ts.close();
            courtesyTitleAnalyzer.close();
        }

    }
    @Override
    protected TokenStreamComponents createComponents(String fieldName) {
        Tokenizer letterTokenizer = new LetterTokenizer();
        TokenStream filter = new CourtesyTitleFilter(letterTokenizer);
        return new TokenStreamComponents(letterTokenizer, filter);
    }
}
```

### output(`dr` and `mrs` will be replaced by `doctor` and `miss`)
```
[doctor]
[kerker]
[and]
[miss]
[GG]
[are]
[couple]
```