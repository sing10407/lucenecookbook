## 2-5 Defining custom TokenFilters

Sometimes the built-in `TokenFilters` can't meets all requirements, like expand abbreviation `dr` to `doctor` and `mrs` to `miss`.

To customize a token filter, you just need to extend `TokenFilter` and @override `incrementToken()`.

Sample code：


```java
public class MyCustomTokenFilters extends TokenFilter {
    Map<String,String> courtesyTitleMap = new HashMap<String,String>();
    private CharTermAttribute termAttr;
    public MyCustomTokenFilters(TokenStream input) {
        super(input);
        termAttr = addAttribute(CharTermAttribute.class);
        courtesyTitleMap.put("dr", "doctor");
        courtesyTitleMap.put("mr", "mister");
        courtesyTitleMap.put("mrs", "miss");
    }
    @Override
    public boolean incrementToken() throws IOException {
        if (!input.incrementToken()) return false;
        String small = termAttr.toString();
        if(courtesyTitleMap.containsKey(small)) {
            termAttr.setEmpty().append(courtesyTitleMap.get(small));
        }
        return true;
    }
}

```

## complete code sample
```java
public class CourtesyTitleFilter extends TokenFilter {
    public static void main(String[] argv) throws IOException {
        StringReader reader = new StringReader("Dr kerker and Mrs GG are couple.");
        StandardAnalyzer standardAnalyzer = new StandardAnalyzer();
        TokenStream ts = null;

        try {
            ts = standardAnalyzer.tokenStream("field", reader);
            // use new token filter
            ts = new CourtesyTitleFilter(ts);

            CharTermAttribute termAtt = ts.addAttribute(CharTermAttribute.class);
            
            ts.reset();
            while (ts.incrementToken()) {
                System.out.println("[" + termAtt.toString() + "]");
            }
            ts.end();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            ts.close();
            standardAnalyzer.close();
        }

    }

    Map<String,String> courtesyTitleMap = new HashMap<String,String>();
    private CharTermAttribute termAttr;
    public CourtesyTitleFilter(TokenStream input) {
        super(input);
        termAttr = addAttribute(CharTermAttribute.class);
        courtesyTitleMap.put("dr", "doctor");
        courtesyTitleMap.put("mr", "mister");
        courtesyTitleMap.put("mrs", "miss");
    }
    @Override
    public boolean incrementToken() throws IOException {
        if (!input.incrementToken()) return false;
        String small = termAttr.toString();
        if(courtesyTitleMap.containsKey(small)) {
            termAttr.setEmpty().append(courtesyTitleMap.get(small));
        }
        return true;
    }
}
```

### output (`dr` and `mrs` will be replaced by `doctor` and `miss`)
```
[doctor]
[kerker]
[and]
[miss]
[gg]
[are]
[couple]
```