
## Obtaining a common analyzer

Five common analyzers Lucene provides in the `lucene-analyzers-common`:

## WhitespaceAnalyzer
Split text an whitespaces

```
[Lucene] [is] [mainly] [used] [for] [information] [retrieval] [and] [you] [can] [read] [more] [about] [it] [at] [lucene.apache.org.]
```

It clear that no normalization has been applied to the text. If this analyzer is used exclusively for both indexing and searching, matches will have to be exact (including matching cases) to be found.



## SimpleAnalyzer
- Split text at non-letter characters
- lowercases resulting tokens

```
[lucene] [is] [mainly] [used] [for] [information] [retrieval] [and] [you] [can] [read] [more] [about] [it] [at] [lucene] [apache] [org] 
```
Tokens are split by no-letters and lowercased in this sample.



## StopAnalyzer
- Split text at non-letter characters
- lowercases resulting tokens
- removes stopwords
    - there are a default set of stopwords
    - you can give StopAnalyzer your own set of stopwords

```
[lucene] [mainly] [used] [information] [retrieval] [you] [can] [read] [more] [about] [lucene] [apache] [org]
```

Common English stopwords are removed and tokens are lowercased and normalized.

## StandardAnalyzer
- Split text using a grammar-based tokenization,normalizes and lowercases tokens, removes stopwords, anddiscards punctuations.
- It can be used to extract company names, e-mail, modelnumbers, and so on.
- great for general usage.

```
[lucene] [mainly] [used] [information] [retrieval] [you] [can] [read] [more] [about] [lucene.apache.org]
```

It uses a different tokenizer and filter called `StandardTokenizer` and `StandardFilter`, tokenizing text by grammar and removing punctuation. 

This analyzer is suitable for special wording such as `product model` and `url`.

By the way, `product model` usually controversial when performing a search, someone think product model only need to be searched when product model is full-matches, and the others think not.

## SnowballAnalyzer

- similar to StandardAnalyzer
- an additional SnowballFilter for stemming.
- Aggressive in stemming, so false positive are possible.
- [Deprecated. in 5.0.  Use the language-specificanalyzer in modules/analysis instead. This analyzer willbe removed in Lucene 5.0](https://lucene.apache.org/core4_0_0/analyzers-common/org/apache/lucene/analysissnowball/SnowballAnalyzer.html)
- For example, `EnglishAnalyzer` (default with `PorterStemmer`)
    
```
[lucen] [is] [main] [use] [for] [inform] [retriev] [and] [you] [can] [read] [more] [about] [it] [at] [lucene.apache.org]
```

Note that some words are changed to their root form, such as `used` to `use`, defined by filter.

