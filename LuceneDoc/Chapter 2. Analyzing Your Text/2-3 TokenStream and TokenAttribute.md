## 2-3 TokenStream

`TokenStream` is an enumeration of tokens, passing between analysis process.

How to use：

```java
Reader reader = new StringReader("Text to be passed"); 
Analyzer analyzer = new SimpleAnalyzer(); 
TokenStream tokenStream = analyzer.tokenStream("myField", reader);
```


## TokenAttribute

We will provide `TokenStream` with one or more attribute objects. *There is only one instance exists per attribute, for performance reasons.*

Basic types of attributes:

- CharTermAttribute : 
This exposes a token's actual textual value, equivalent to a term's value. 

- PositionIncrementAttribute : 
This returns the position of the current token relative to the previous token. *This attribute is useful in phrase-matching as the keyword order and their positions are important.*  If there are no gaps between the current token and the previous token (for example, no stop words in between), it will be set to its default value, 1. 

- OffsetAttribute : 
This gives you information about the start and end positions of the corresponding term in the source text. 

- TypeAttribute : 
This is available if it is used in the implementation. This is usually used to identify the data type.

Example：

```java
StringReader reader = new StringReader("Lucene is mainly used for information retrieval and you can read more about it at lucene.apache.org."); 
StandardAnalyzer wa = new StandardAnalyzer(); 
TokenStream ts = null; 

try { 
    ts = wa.tokenStream("field", reader); 

    OffsetAttribute offsetAtt = ts.addAttribute(OffsetAttribute.class); 
    CharTermAttribute termAtt = ts.addAttribute(CharTermAttribute.class); 

    // reset TokenStream to the beginning.
    ts.reset(); 
    while (ts.incrementToken()) { 
        String token = termAtt.toString(); 

        System.out.println("[" + token + "]"); 
        System.out.println(" Token starting offset: " + offsetAtt.startOffset()); 
        System.out.println(" Token ending offset: " + offsetAtt.endOffset()); 
        System.out.println(""); 
    } 
    ts.end(); 
} catch (IOException e) { 
    e.printStackTrace(); 
} finally { 
    ts.close(); 
    wa.close(); 
}
```

output:
```
[lucene]
 Token starting offset: 0
 Token ending offset: 6

[is]
 Token starting offset: 7
 Token ending offset: 9

[mainly]
 Token starting offset: 10
 Token ending offset: 16

[used]
 Token starting offset: 17
 Token ending offset: 21

[for]
 Token starting offset: 22
 Token ending offset: 25

[information]
 Token starting offset: 26
 Token ending offset: 37

[retrieval]
 Token starting offset: 38
 Token ending offset: 47

[and]
 Token starting offset: 48
 Token ending offset: 51

[you]
 Token starting offset: 52
 Token ending offset: 55

[can]
 Token starting offset: 56
 Token ending offset: 59

[read]
 Token starting offset: 60
 Token ending offset: 64

[more]
 Token starting offset: 65
 Token ending offset: 69

[about]
 Token starting offset: 70
 Token ending offset: 75

[it]
 Token starting offset: 76
 Token ending offset: 78

[at]
 Token starting offset: 79
 Token ending offset: 81

[lucene.apache.org]
 Token starting offset: 82
 Token ending offset: 99

```

using `PositionIncrementAttribute` to implement `StopWordFilter`

The `PositionIncrementAttribute` class shows the position of the current token relative to the previous token. The default value is 1.

This attribute is important for phrase matching, like `stopword filtering` or `synonym matching` .

```java
public class MyStopWordFilter extends TokenFilter {
    public static void main(String[] argv) throws IOException {
        StringReader reader = new StringReader("Lucene is mainly used for information retrieval and you can read more about it at lucene.apache.org.");
        StandardAnalyzer wa = new StandardAnalyzer();
        TokenStream ts = null;

        try {
            ts = wa.tokenStream("field", reader);

            ts =  new MyStopWordFilter(ts);  // filter stop words

            CharTermAttribute termAtt = ts.addAttribute(CharTermAttribute.class);
            PositionIncrementAttribute positionIncriAtt = ts.addAttribute(PositionIncrementAttribute.class);

            // reset TokenStream to the beginning.
            ts.reset();
            while (ts.incrementToken()) {
                String token = termAtt.toString();

                System.out.println("[" + token + "]");
                // greater 1 implies the previous token and current token are not consecutive
                System.out.println(" Token PositionIncrement offset: " + positionIncriAtt.getPositionIncrement());
                System.out.println("");
            }
            ts.end();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            ts.close();
            wa.close();
        }
    }


    private CharTermAttribute charTermAtt;
    public PositionIncrementAttribute posIncrAtt;

    public MyStopWordFilter(TokenStream input) {
        super(input);
        charTermAtt = addAttribute(CharTermAttribute.class);
        posIncrAtt = addAttribute(PositionIncrementAttribute.class);
    }

    @Override
    public boolean incrementToken() throws IOException {
        int extraIncrement = 0;
        boolean returnValue = false;
        while (input.incrementToken()) {
            if (ENGLISH_STOP_WORDS_SET.contains(charTermAtt.toString())) {
                extraIncrement++;// filter this word
                continue;
            }
            returnValue = true;
            break;
        }
        // have stop words
        if (extraIncrement > 0) {
            posIncrAtt.setPositionIncrement(posIncrAtt.getPositionIncrement() + extraIncrement);
        }
        return returnValue;
    }
}
```

output:
```
[lucene]
 Token PositionIncrement offset: 1

[mainly]
 Token PositionIncrement offset: 2

[used]
 Token PositionIncrement offset: 1

[information]
 Token PositionIncrement offset: 2

[retrieval]
 Token PositionIncrement offset: 1

[you]
 Token PositionIncrement offset: 2

[can]
 Token PositionIncrement offset: 1

[read]
 Token PositionIncrement offset: 1

[more]
 Token PositionIncrement offset: 1

[about]
 Token PositionIncrement offset: 1

[lucene.apache.org]
 Token PositionIncrement offset: 3
```