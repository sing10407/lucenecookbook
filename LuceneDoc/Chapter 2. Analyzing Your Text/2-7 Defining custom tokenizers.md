## Defining custom tokenizers

Lucene provides a character-based tokenizer `CharTokenizer` that should be suitable for most types of tokenizations. `LetterTokenizer` and `WhitespaceTokenizer` both extend from `CharTokenizer`.

Override `isTokenChar()` to determine what characters should be considered as part of a token and what characters should be considered as delimiters. 


### Simple tokenizer split by space：

```java
public class MyTokenizer extends CharTokenizer {
    public MyTokenizer() {
        super();
    }

    public MyTokenizer(AttributeFactory factory) {
        super(factory);
    }

    @Override
    protected boolean isTokenChar(int c) {

        return !Character.isSpaceChar(c);
    }
}
```

### Complete code sample
```java
public class MyTokenizer extends CharTokenizer {
    public static void main(String[] argv) throws IOException {
        StringReader reader = new StringReader("test a b c gg-5566.");
        // custom tokenizer
        MyTokenizer tokenizer = new MyTokenizer();
        // inject tokenizer into a custom analyzer
        CourtesyTitleAnalyzer courtesyTitleAnalyzer = new CourtesyTitleAnalyzer(tokenizer);
        TokenStream ts = null;

        try {
            ts = courtesyTitleAnalyzer.tokenStream("field", reader);
            CharTermAttribute termAtt = ts.addAttribute(CharTermAttribute.class);

            ts.reset();
            while (ts.incrementToken()) {
                System.out.println("[" + termAtt.toString() + "]");
            }
            ts.end();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            ts.close();
            courtesyTitleAnalyzer.close();
        }
    }
    public MyTokenizer() {
        super();
    }
    public MyTokenizer(AttributeFactory factory) {
        super(factory);
    }
    @Override
    protected boolean isTokenChar(int c) {
        return !Character.isSpaceChar(c);
    }
}
```


### Output
```
[test]
[a]
[b]
[c]
[gg-5566.]

```