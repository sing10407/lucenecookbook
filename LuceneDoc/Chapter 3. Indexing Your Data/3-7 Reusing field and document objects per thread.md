## Reusing field and document objects per thread

There are some best practices in using Lucene, one of the best practices is to reuse both the Document and field objects. This avoids the cost of creating object, and will reduce the chance of java GC.

```java
Document doc = new Document();
StringField stringField = new StringField("name", "", Field.Store.YES);

// re-use Document() and StringField()
String[] names = {"John", "Mary", "Peter"};
for (String name : names) {
    stringField.setStringValue(name);
    doc.removeField("name");
    doc.add(stringField);
    indexWriter.addDocument(doc);
}

indexWriter.commit();

```

### complete sample
```java
public class MyReUseDocument {
    public  static void main(String[] argv) throws IOException {
        Analyzer analyzer = new StandardAnalyzer();
        Directory directory = new MMapDirectory(new File("gg").toPath());
        IndexWriterConfig config = new IndexWriterConfig( analyzer);
        IndexWriter indexWriter = new IndexWriter(directory, config);

        Document doc = new Document();
        StringField stringField = new StringField("name", "", Field.Store.YES);

        // re-use Document() and StringField()
        String[] names = {"John", "Mary", "Peter"};
        for (String name : names) {
            stringField.setStringValue(name);
            doc.removeField("name");
            doc.add(stringField);
            indexWriter.addDocument(doc);
        }

        indexWriter.commit();
        IndexReader reader = DirectoryReader.open(directory);
        for (int i = 0; i < 3; i++) {
            doc = reader.document(i);
            System.out.println("DocId: " + i + ", name: " + doc.getField("name").stringValue());
        }
    }
}
```

### result
```
DocId: 0, name: John
DocId: 1, name: Mary
DocId: 2, name: Peter

```
