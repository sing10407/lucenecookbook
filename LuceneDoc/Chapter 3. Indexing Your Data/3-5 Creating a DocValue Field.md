## 3-5 Creating a DocValue Field

In original stored `field` is `row-oriented`, in retrieval, all field values are returened at once per document, so loading each field per document is very fast, **but when you need to scan certain field, you have to iterate and load each field through all documents.**



### Field (row-oriented)
| docid | name  | gender | age |
| ----- | ----- | ------ | --- |
| 1     | John  | M      | 30  |
| 2     | Mary  | F      | 25  |
| 3     | Peter | M      | 40  |


The `DocVelue` is stored by column in `DocId` to `value` mapping, and loading the values for a specific DocValue for all documents at once can be done quickly.

### DocValue (column-oriented)
| docid | value |
| ----- | ----- |
| 1     | John  |
| 2     | Mary  |
| 3     | Peter |

| docid | value |
| ----- | ----- |
| 1     | M     |
| 2     | F     |
| 3     | M     |

| docid | value |
| ----- | ----- |
| 1     | 30    |
| 2     | 25    |
| 3     | 40    |

In summary, the `field` and `DocValue` both contain information about a document, but they serve a different purpose in practical usage.


Lucene provides the following DocValue types: 

- `BinaryDocValues` : This is a per-document byte[] array that can be used to store custom data structure 
- `NumericDocValues` : This is a per-document single-valued numeric type value 
- `SortedDocValues` : This is a per-document single-valued `string type` **that's stored and sorted separately** ; the DocValue itself is a DocId to term ID mapping where the term ID references a term in a sorted term list 
- `SortedNumericDocValues` : This is similar to SortedDocValues , but this is for numeric values only 
- `SortedSetDocValues` : This is similar to SortedDocValues , but each document in DocValues is mapped to a set instead of a single value.


### Usage

```java
Document document = new Document();

// Add DocValues
document.add(
    new SortedDocValuesField(
        "sorted_string", 
        new BytesRef("hello")
    )
);

indexWriter.addDocument(document);
indexWriter.commit();
```


### Complete sample

Note: `AtomicReader` and `AtomicReaderContext` are now called `LeafReader` and `LeafReaderContext`, respectively.

https://lucene.apache.org/core/5_0_0/MIGRATE.html

```java
public class MyDocValueField {
    public static void main(String[] argv) throws IOException {
        Analyzer analyzer = new StandardAnalyzer();
        Directory directory = new MMapDirectory(new File("gg").toPath());
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        IndexWriter indexWriter = new IndexWriter(directory, config);
        Document document = new Document();

        // Add DocValues
        document.add(new SortedDocValuesField("sorted_string", new BytesRef("hello")));
        indexWriter.addDocument(document);
        document = new Document();
        document.add(new SortedDocValuesField("sorted_string", new BytesRef("world")));
        indexWriter.addDocument(document);

        indexWriter.commit();
        indexWriter.close();

        IndexReader reader = DirectoryReader.open(directory);
        document = reader.document(0);
        System.out.println("doc 0: " + document.toString());
        document = reader.document(1);
        System.out.println("doc 1: " + document.toString());

        for (LeafReaderContext context : reader.leaves()) {
            LeafReader atomicReader = context.reader();
            SortedDocValues sortedDocValues = DocValues.getSorted(atomicReader, "sorted_string");
            System.out.println("Value count: " + sortedDocValues.getValueCount());
            System.out.println("doc 0 sorted_string: " + sortedDocValues.lookupOrd(0).utf8ToString());
            System.out.println("doc 1 sorted_string: " + sortedDocValues.lookupOrd(1).utf8ToString());
        }
        reader.close();
    }
}
```

### output

```
doc 0: Document<>
doc 1: Document<>
Value count: 2
doc 0 sorted_string: hello
doc 1 sorted_string: world
```