## Changing similarity implementation used during indexing

Part of the norms calculation at the index time is similarity. Lucene has already implemented a complex model called `TFIDFSimilarity` as a default calculation for norms.

* TFIDFSimilarity
  * http://lucene.apache.org/core/8_0_0/core/org/apache/lucene/search/similarities/TFIDFSimilarity.html
* tf-idf
  * https://en.wikipedia.org/wiki/Tf%E2%80%93idf
  



```java
IndexWriterConfig config = new IndexWriterConfig(analyzer);

// DefaultSimilarity has been renamed to ClassicSimilarity to prepare for the move to BM25 in Lucene 6
MySimilarity similarity = new ClassicSimilarity();
config.setSimilarity(similarity);
```

---
### complete sample

```java
public class MySimilarity {

    public static void main(String[] argv) throws IOException {

        Analyzer analyzer = new StandardAnalyzer();
        Directory directory = new RAMDirectory();
        IndexWriterConfig config = new IndexWriterConfig(analyzer);

        // DefaultSimilarity has been renamed to ClassicSimilarity to prepare for the move to BM25 in Lucene 6
        ClassicSimilarity similarity = new ClassicSimilarity();
        config.setSimilarity(similarity);
        IndexWriter indexWriter = new IndexWriter(directory, config);

        Document doc = new Document();
        TextField textField = new TextField("name", "", Field.Store.YES);
        NumericDocValuesField docValuesField = new NumericDocValuesField("ranking", 1);

        long ranking = 1l;
        String[] names = {"John R Smith", "Mary Smith", "Peter Smith"};
        for (String name : names) {
            ranking *= 2;
            textField.setStringValue(name);
            docValuesField.setLongValue(ranking);
            doc.removeField("name");
            doc.removeField("ranking");
            doc.add(textField);
            doc.add(docValuesField);
            indexWriter.addDocument(doc);
        }
        indexWriter.commit();

        IndexReader indexReader = DirectoryReader.open(directory);
        IndexSearcher indexSearcher = new IndexSearcher(indexReader);
        indexSearcher.setSimilarity(similarity);

        Query query = new TermQuery(new Term("name", "smith"));
        TopDocs topDocs = indexSearcher.search(query, 100);
        System.out.println("Searching 'smith'");
        for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
            doc = indexReader.document(scoreDoc.doc);
            System.out.println(doc.getField("name").stringValue());
        }
    }
}
```

### output

```
Searching 'smith'
Mary Smith
Peter Smith
John R Smith
```


---

Note:
`SimScorer` now only takes a frequency and a norm as per-document scoring factors. 
- https://lucene.apache.org/core/8_0_0/changes/Changes.html#v8.0.0.api_changes
- http://lucene.apache.org/core/8_0_0/core/org/apache/lucene/search/similarities/Similarity.html?is-external=true
