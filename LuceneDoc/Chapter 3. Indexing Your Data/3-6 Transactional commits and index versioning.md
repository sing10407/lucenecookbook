## Transactional commits and index versioning

The concept of `ACID`：
- `Atomicity`： 
    - This properity requires that **each transaction is all or nothing.** When a transaction failed, none of the partial change will be available for search.
    - Lucene `IndexWriter` supports transactional `commit()`.
- `Consistency`：
    - This properity ensures that any committed changes will bring system from one valid state to another valid state.
    - Lucene index is always valid and the uncommitted transactions will never be mad visible.
    - Lucene index can survive crashes, a power failure.
- `Isolation`：
    - Concurrent execution of transactions is supported and each transaction runs it own isolation, without interfering each other.
    - Lucene don't exposing the changes to `IndexReader` while in transaction until `indexWriter.commit()` called.
- `Durability`:
    - This properity ensures that the changes ramain intact once committed, the changes are persisted on the disk.


Lucene supports two-phase commit by call `prepareCommit()` in `IndexWritter`. Then you can call `commit()` to commit the changes or `rollback()` to rollback the transaction. 

Note that calling `rollback()` will also close the `IndexWriter` . Calling `commit()` actually triggers a call to `prepareCommit()` internally and calling `close()` will trigger a call to `commit()` before `IndexWriter` is closed.

### IndexDeletionPolicy

Each commit in IndexWriter (`IndexCommits`) can be considered as a snapshot of the index. 

Lucene default uses `IndexDeletionPolicy` that would delete prior `IndexCommits` after a commit.

`IndexDeletionPolicy` contains：
- KeepOnlyLastCommitDeletionPolicy
    - https://lucene.apache.org/core/7_0_1/core/org/apache/lucene/index/KeepOnlyLastCommitDeletionPolicy.html
    - This IndexDeletionPolicy implementation that keeps only the most recent commit and immediately removes all prior commits after a new commit is done. This is the default deletion policy.
- NoDeletionPolicy
    - https://lucene.apache.org/core/7_0_1/core/org/apache/lucene/index/NoDeletionPolicy.html
    - An IndexDeletionPolicy which keeps all index commits around, never deleting them. This class is a singleton and can be accessed by referencing INSTANCE.
- SnapshotDeletionPolicy
    - https://lucene.apache.org/core/7_0_1/core/org/apache/lucene/index/SnapshotDeletionPolicy.html
    - An IndexDeletionPolicy that wraps any other IndexDeletionPolicy and adds the ability to hold and later release snapshots of an index. While a snapshot is held, the IndexWriter will not remove any files associated with it even if the index is otherwise being actively, arbitrarily changed. Because we wrap another arbitrary IndexDeletionPolicy, this gives you the freedom to continue using whatever IndexDeletionPolicy you would normally want to use with your index.
    - This class maintains all snapshots in-memory, and so the information is not persisted and not protected against system failures. If persistence is important, you can use 
- IndexDeletionPolicy
    - https://lucene.apache.org/core/7_0_1/core/org/apache/lucene/index/IndexDeletionPolicy.html
    - Implement this interface, and pass it to one of the IndexWriter or IndexReader constructors, to customize when older point-in-time commits are deleted from the index directory. The default deletion policy is KeepOnlyLastCommitDeletionPolicy, which always removes old commits as soon as a new commit is done (this matches the behavior before 2.2).
- PersistentSnapshotDeletionPolicy
    - https://lucene.apache.org/core/7_0_1/core/org/apache/lucene/index/PersistentSnapshotDeletionPolicy.html
    - A SnapshotDeletionPolicy which adds a persistence layer so that snapshots can be maintained across the life of an application. The snapshots are persisted in a Directory and are committed as soon as snapshot() or release(IndexCommit) is called.



### Sample code

```java
public class MyIndexTransaction {
    public static void main(String[] argv) throws IOException {

        Analyzer analyzer = new StandardAnalyzer();
        Directory directory = new MMapDirectory(new File("gg").toPath());
        IndexWriterConfig config = new IndexWriterConfig(analyzer);

        // use SnapshotDeletionPolicy
        // SEE https://lucene.apache.org/core/7_0_1/core/org/apache/lucene/index/SnapshotDeletionPolicy.html
        SnapshotDeletionPolicy policy = new SnapshotDeletionPolicy(NoDeletionPolicy.INSTANCE);
        config.setIndexDeletionPolicy(policy);

        IndexWriter indexWriter = new IndexWriter(directory, config);

        IndexCommit lastSnapshot;
        Document document = new Document();
        indexWriter.addDocument(document);
        indexWriter.commit();

        lastSnapshot = policy.snapshot();
        document = new Document();
        indexWriter.addDocument(document);
        indexWriter.commit();

        lastSnapshot = policy.snapshot();
        document = new Document();
        indexWriter.addDocument(document);
        indexWriter.rollback();
        indexWriter.close();

        List<IndexCommit> commits = DirectoryReader.listCommits(directory);
        System.out.println("Commits count: " + commits.size());

        for (IndexCommit commit : commits) {
            IndexReader reader = DirectoryReader.open(commit);
            System.out.println("Commit "+ commit.getSegmentCount());
            System.out.println("Number of docs: " + reader.numDocs());
        }

        System.out.println("");
        System.out.println("Snapshots count: " + policy.getSnapshotCount());
        List<IndexCommit> snapshots = policy.getSnapshots();

        for (IndexCommit snapshot : snapshots) {
            IndexReader reader = DirectoryReader.open(snapshot);
            System.out.println("Snapshot " + snapshot.getSegmentCount());
            System.out.println("Number of docs: " + reader.numDocs());
        }

        policy.release(lastSnapshot);
        System.out.println("\nSnapshots count: " + policy.getSnapshotCount());
    }
}
```

### output
```
Commits count: 2
Commit 1
Number of docs: 1
Commit 2
Number of docs: 2

Snapshots count: 2
Snapshot 1
Number of docs: 1
Snapshot 2
Number of docs: 2

Snapshots count: 1
```