## 3-8 Delving into field norms

How we ranking a result is giving each item a score in search result, higher score means higher relevance. A `norm` is part of the calculation of score that is used to measure relevance.

Lucene allows 2 types of boosting:

- index time boost
  - **Index-time boosts are deprecated**
  - https://lucene.apache.org/core/6_5_1/core/org/apache/lucene/document/Field.html
- query time boost

---

`Query time boost` will be mentioned in later chapters.