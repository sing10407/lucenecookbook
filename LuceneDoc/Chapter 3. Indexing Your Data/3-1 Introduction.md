## Introduction

We will cover the following topics in this chapter: 

- Obtaining an IndexWriter 
- Creating a StringField 
- Creating a TextField 
- Creating a numeric field 
- Creating a DocValue field 
- Transactional commits and index versioning 
- Reusing field and document objects per thread 
- Delving into field norms 
- Changing similarity implementation used during indexing

---
## Basic Concept of `Inverted Index`

### Dataset：
| Docid | text                                         |
| ----- | -------------------------------------------- |
| 1     | Humpty Dumpty sat on a wall.                 |
| 2     | Humpty Dumpty had a great fall.              |
| 3     | All the king's horses and all the king's men |
| 4     | Couldn't put Humpty together again           |


↓↓↓

### Analyzer with stopword filter

↓↓↓

### Inverted Index

| Term     | Docid |
| -------- | ----- |
| again    | 4     |
| all      | 3     |
| couldn't | 4     |
| dumply   | 1,2   |
| fall     | 2     |
| great    | 2     |
| had      | 2     |
| horses   | 3     |
| humply   | 1,2,4 |
| king's   | 3     |
| men      | 3     |
| put      | 4     |
| sat      | 1     |
| together | 4     |
| wall     | 1     |

The `inverted index` contains a sorted list of terms with an associated DocId for each term. The term to DocId relationship is a one-to-many relationship. 

---
## The segmented nature of Lucene's index file format
Another important concept to learn is the segmented nature of Lucene's index file format. Index files are physically separated by segments and they follow a naming standard, where files are named `segments_1` , then `segments_2` , and so on. All files belonging to the same segment share the same filename with varying extensions. 

Each segment is a fully independent index that can be searched separately. Simply adding new documents can generate a new segment.

**The terms and documents can exist in any of the segments, depending on when the new documents and terms are added.** The `IndexReader` attributes will merge the index segments internally so that you can access a coherent index without having to consider the index segmentation.

![](../images/segment.jpg)