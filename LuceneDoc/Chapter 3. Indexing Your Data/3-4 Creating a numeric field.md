
## Creating a numeric field

Lucene provides 4 Field classes for numeric values.
- IntField
- FloatField
- LongField
- DoubleField

Lucene also storing numeric data as `trie tree`(also called ordered tree). 

```mermaid
graph TD
    root((root))-->1((1))
    root((root))-->2((2))
    root((root))-->3((3))

    1-->12((12))
    1-->15((15))

    2-->23((23))
    2-->24((24))

    3-->32((32))
    3-->37((37))

    12-->121((121))
    12-->123((123))
    12-->127((127))

    15-->153((153))

    23-->232((232))
    23-->234((234))

    37-->372((372))
    37-->375((375))
    37-->377((377))
```

Each parent node are child node that divided by 10. So if you want to search numeric value between 230 and 239, and you just need to find the 23 bracket in the index and return all the `DocIds` underneath.

This technique allows Lucene to quickly search by a numeric range.

Numeric values can be 
- sorted
- searched by range
- matched exactly




```java
// int field
IntPoint intField = new IntPoint("int_value", 100);
// if store the value (value included in search result or not)
StoredField storedField = new StoredField("int_value", 100);

LongPoint longField = new LongPoint("long_value", 100L);
FloatPoint floatField = new FloatPoint("float_value", 100.0F);
DoublePoint doubleField = new DoublePoint("double_value", 100.0D);

document.add(intField);
document.add(storedField);
document.add(longField);
document.add(floatField);
document.add(doubleField);
indexWriter.addDocument(document);
```
---

### Deprecated Class
`IntField`, `LongField`, `FloatField`, `DoubleField` are Deprecated. 

Use `IntPoint`, `LongPoint`, `FloatPoint`, `DoublePoint` instead.

[See this.](http://lucene.apache.org/core/7_0_1/core/org/apache/lucene/index/PointValues.html?is-external=true)

### Deprecated method
↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓↓

The number of brackets(child nodes) can be tuned by changing the value called `precisionStep`. A smaller `PrecisionStep` will result in larger number of brackets that will consume more disk space and **improve the search range performance**.

Note：If you intend to sort by a numeric field, you should create a separate single-value field to sort purposes.(setting `precisionStep` to `Integer.MAX_VALUE`). It will be more efficient than using the bracketed index.
