## Obtaining an IndexWriter

You can use `IndexWriterConfig` to set attributes to `IndexWriter`. `IndexWriterConfig` provides 3 `OpenMode` options to open and index.

- APPEND
  - Opening an existing index on a directory and allowing `IndexWriter` to update index.
  - `IndexNotFoundException` will be thrown when no index found.
- CREATE
  - This creates a new index, if a directory does not contain one already.
  - **it replaces existing index if there are already have existing index.**
- CREATE_OR_APPEND
  - default option
  - Safe option
  - If no index is found, use `CREATE`. Otherwise use `APPEND`.


### Sample code：
```java
FSDirectory directory = FSDirectory.open(new File("gg").toPath());
Analyzer analyzer = new StandardAnalyzer();

IndexWriterConfig config = new IndexWriterConfig(analyzer);
config.setOpenMode(IndexWriterConfig.OpenMode.CREATE); // Configure OpenMode here
config.setRAMBufferSizeMB(64);
config.setMaxBufferedDocs(4000);

IndexWriter indexWriter = new IndexWriter(directory, config);
```

And you can find the file `write.lock` was generate under the folder `gg`.

### Extra configuration：
- setRAMBufferSizeMB
  - RAM size for buffering before flushed to the directory.
  - default 16 MB.
- setMaxBufferedDocs
  - **Those docs was flushed as a new segment when reach the configure.**

### Note 
```
Note that the changes to index are not visible until commit() or close() is called on Index Writer .
```