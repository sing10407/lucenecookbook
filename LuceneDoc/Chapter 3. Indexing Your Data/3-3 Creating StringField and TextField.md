
## Creating a StringField

They are part of a document containing information about the document. A field is composed of three parts: `name`, `type`, and `value`. Values can be text, binary, or numeric.

Note that when you use `StringField`, **the value will be indexed, but not tokenized.** So a query seeking an exact match of the value.

The situation that suitable for use `StringField` are like `phone number`, `area code`...etc.


```java
Document document = new Document();

// String Field
document.add(
    new StringField(
        "telephone_number", // field name
        "04735264927",  // value
        Field.Store.YES // value included in search result or not
    )
);

indexWriter.addDocument(document);
indexWriter.commit();
```


## Creating a TextField

The major differences between `StringField` and `TextField` are `TextField` will be tokenized by Analyzer.

```java
Document document = new Document();

// Text Field
String text = "Lucene is an Information Retrieval library written in Java.";
document.add(
    new TextField("text", text, Field.Store.YES)
);

indexWriter.addDocument(document);
indexWriter.commit();
```