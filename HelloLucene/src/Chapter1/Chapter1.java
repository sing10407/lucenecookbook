package Chapter1;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;

import java.io.IOException;

public class Chapter1 {
    public static void main(String[] args) throws Exception {

        StandardAnalyzer analyzer = new StandardAnalyzer();
        Directory index = new RAMDirectory();
        IndexWriterConfig config = new IndexWriterConfig(analyzer);

        // write documents to index
        IndexWriter indexWritter = new IndexWriter(index, config);

        // create a document object
        Document doc = new Document();
        String text = "Lucene is an Information Retrieval library written in java";
        doc.add(new TextField("Content", text, Field.Store.YES));

        indexWritter.addDocument(doc);
        indexWritter.close();

        // Create Query object, use "Lucene" as keyword and search on field "Content"
        Query query = new QueryParser("Content", analyzer).parse("Lucene");

        int hitsPerPage = 10;

        // get a searcher
        IndexReader reader = DirectoryReader.open(index);
        IndexSearcher searcher = new IndexSearcher(reader);

        // perform search (search on index created before)
        TopDocs docs = searcher.search(query, hitsPerPage);
        ScoreDoc[] hits = docs.scoreDocs;

        // display results
        System.out.println("Found " + hits.length + " hits.");
        for (int i = 0; i < hits.length; ++i) {
            int docId = hits[i].doc; //get doc id
            Document d = searcher.doc(docId);
            System.out.println(d.get("Content"));
        }
    }

}

