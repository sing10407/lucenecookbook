package Chapter4;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.MMapDirectory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.IOException;

public class SpecifyingSortLogic {
    public static void main(String[] argv) throws IOException {

        StandardAnalyzer analyzer = new StandardAnalyzer();
        Directory directory = new RAMDirectory();
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        IndexWriter indexWriter = new IndexWriter(directory, config);



        String[] contents = {"foxtrot", "echo", "delta", "charlie", "bravo", "alpha"};
        for (String content : contents) {
            Document doc = new Document();

            StringField stringField = new StringField("name", content, Field.Store.YES);
            doc.add(stringField);

            // you need too create SortedDocValuesField if you want to sort by field
            SortedDocValuesField sortedDocValuesField = new SortedDocValuesField("name", new BytesRef(content));
            doc.add(sortedDocValuesField);

            indexWriter.addDocument(doc);
        }
        indexWriter.commit();

        IndexReader indexReader = DirectoryReader.open(directory);
        IndexSearcher indexSearcher = new IndexSearcher(indexReader);

        WildcardQuery query = new WildcardQuery(new Term("name",new BytesRef("*")));
        Sort sort = new Sort(
                new SortField("name", SortField.Type.STRING)
        );
        TopDocs topDocs = indexSearcher.search(query, 10, sort);
        for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
            Document doc = indexReader.document(scoreDoc.doc);
            System.out.println(scoreDoc.score + ": " + doc.getField("name").stringValue() );
        }
    }
}
