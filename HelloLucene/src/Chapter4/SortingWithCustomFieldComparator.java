package Chapter4;

import org.apache.lucene.index.BinaryDocValues;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.search.FieldComparator;
import org.apache.lucene.search.LeafFieldComparator;

import java.io.IOException;

public class SortingWithCustomFieldComparator extends FieldComparator<String> {
    private String field;
    private String bottom;
    private String topValue;
    private BinaryDocValues cache;
    private String[] values;

    public SortingWithCustomFieldComparator(String field, int numHits) {
        this.field = field;
        this.values = new String[numHits];
    }

    public int compare(int slot1, int slot2) {
        return compareValues(values[slot1],values[slot2]);
    }

    public int compareValues(String first, String second) {
        int val = first.length() - second.length();
        return val == 0 ? first.compareTo(second) : val;
    }

    public void setTopValue(String value) {
        this.topValue = value;
    }

    public String value(int slot) {
        return values[slot];
    }

    @Override
    public LeafFieldComparator getLeafComparator(LeafReaderContext leafReaderContext) throws IOException {
        //this.cache = FieldCache.DEFAULT.getTerms( leafReaderContext.reader(), field, true);
        return null;
    }
}

