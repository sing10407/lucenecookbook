package Chapter4;

import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.LeafReader;
import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class MyIndexReader {
    public static void main(String[] argv) throws IOException {
        // open a directory
        Directory directory = FSDirectory.open( new File("/data/index").toPath());
        // set up a DirectoryReader
        DirectoryReader directoryReader = DirectoryReader.open(directory);

        // open another DirectoryReader by calling openIfChanged
        DirectoryReader newDirectoryReader = DirectoryReader.openIfChanged(directoryReader);
        // assign newDirectoryReader
        if (newDirectoryReader != null) {
            IndexSearcher indexSearcher = new IndexSearcher(newDirectoryReader);
            // close the old DirectoryReader
            directoryReader.close();
        }
    }
}
