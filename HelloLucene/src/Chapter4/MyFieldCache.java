package Chapter4;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.BytesRef;
import org.apache.lucene.util.Version;

import java.io.IOException;

public class MyFieldCache {
    public static void main(String[] argv) throws IOException {

        StandardAnalyzer analyzer = new StandardAnalyzer();
        Directory directory = new RAMDirectory();
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        IndexWriter indexWriter = new IndexWriter(directory, config);
        Document doc = new Document();
        StringField stringField = new StringField("name", "", Field.Store.YES);
        String[] contents = {"alpha", "bravo", "charlie", "delta", "echo", "foxtrot"};
        for (String content : contents) {
            stringField.setStringValue(content);
            doc.removeField("name");
            doc.add(stringField);
            indexWriter.addDocument(doc);
        }
        indexWriter.commit();
        IndexReader indexReader = DirectoryReader.open(directory);


        // To solve the problem of finding a field value by DocId, Lucene introduced FieldCache

        // https://lucene.apache.org/core/corenews.html
        // FieldCache is gone (moved to a dedicated UninvertingReader in the misc module).
        // This means when you intend to sort on a field,
        // you should index that field using doc values, which is much faster and less heap consuming than FieldCache.

        /*
        BinaryDocValues cache = FieldCache.DEFAULT.getTerms( SlowCompositeReaderWrapper.wrap(indexReader), "name", true);
        for (int i = 0; i < indexReader.maxDoc(); i++) {
            BytesRef bytesRef = cache.get(i);
            System.out.println(i + ": " + bytesRef.utf8ToString());
        }
         */
    }
}
