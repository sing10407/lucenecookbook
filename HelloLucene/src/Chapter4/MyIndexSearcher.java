package Chapter4;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TermQuery;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

import java.io.File;
import java.io.IOException;

public class MyIndexSearcher {
    public static void main(String[] argv) throws IOException {

        // IndexSearcher provides a number of search methods for querying data and returning TopDocs as results.
        // TopDocs represents hits from a search and contains an array of ScoreDoc
        // where it contains DocId and the score of each matching document.
        // Note that TopDocs contains DocId and does not actually contain any document content.
        // Document content retrieval will have to be relied on IndexReader or FieldCache
        Directory directory = FSDirectory.open(new File("/data/index").toPath());
        DirectoryReader directoryReader =DirectoryReader.open(directory);
        IndexSearcher indexSearcher = new IndexSearcher(directoryReader);
    }

    public static void QuerySample1() throws IOException {
        Directory directory = FSDirectory.open(new File("/data/index").toPath());
        IndexReader indexReader = DirectoryReader.open(directory);
        IndexSearcher indexSearcher = new IndexSearcher(indexReader);

        // query on certain field
        Query query = new TermQuery(new Term("content", "alpha"));
        TopDocs topDocs = indexSearcher.search(query, 100);
    }
    public static void QuerySample2() throws IOException, ParseException {
        Directory directory = FSDirectory.open(new File("/data/index").toPath());
        Analyzer analyzer = new StandardAnalyzer();
        IndexReader indexReader = DirectoryReader.open(directory);
        IndexSearcher indexSearcher = new IndexSearcher(indexReader);

        // use an analyzer to tokenize a query string
        // This is done so that we can apply the same text analysis treatment as in the indexing process,
        // to ensure accuracy of the search.
        QueryParser queryParser = new QueryParser("content", analyzer);
        Query query = queryParser.parse("alpha beta");
        TopDocs topDocs = indexSearcher.search(query, 100);
    }
}
