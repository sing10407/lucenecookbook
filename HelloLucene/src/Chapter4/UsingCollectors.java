package Chapter4;

import org.apache.lucene.index.LeafReaderContext;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.ScoreMode;
import org.apache.lucene.search.Scorer;
import org.apache.lucene.search.SimpleCollector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

// https://lucene.apache.org/core/5_3_0/MIGRATE.html
// The Collector API has been refactored to use a different Collector instance per segment.
// It is possible to migrate existing collectors painlessly by extending SimpleCollector instead of Collector:
// SimpleCollector is a specialization of Collector that returns itself as a per-segment Collector.


/*
* http://lucene.apache.org/core/8_0_0/core/org/apache/lucene/search/SimpleCollector.html
* Normally, you shouldn't have to mess with Collector.
* However, on a rare occasion when you need more control over how the matched documents are scored,
* sorted, or even filtered, Collector can be a good place to work on the customization.
* This is assuming that you have already exhausted the options of custom filter, similarity, and/or analyzer.
* */
public class UsingCollectors extends SimpleCollector {
    private int totalHits = 0;
    private int docBase;
    private Scorer scorer;
    private List<ScoreDoc> topDocs = new ArrayList();
    private ScoreDoc[] scoreDocs;

    public UsingCollectors() { }

    public void setScorer(Scorer scorer) {
        this.scorer = scorer;
    }

    @Override
    public void collect(int doc) throws IOException {
        float score = scorer.score();
        if (score > 0) {
            score += (1 / (doc + 1));
        }
        ScoreDoc scoreDoc = new ScoreDoc(doc + docBase, score);
        topDocs.add(scoreDoc);
        totalHits++;
    }

    @Override
    public void doSetNextReader(LeafReaderContext context) {
        this.docBase = context.docBase;
    }

    public int getTotalHits() {
        return totalHits;
    }

    public ScoreDoc[] getScoreDocs() {
        if (scoreDocs != null) {
            return scoreDocs;
        }
        Collections.sort(topDocs,
                new Comparator<ScoreDoc>() {
                    public int compare(ScoreDoc d1, ScoreDoc d2) {
                        if (d1.score > d2.score) {
                            return -1;
                        } else if (d1.score == d2.score) {
                            return 0;
                        } else {
                            return 1;
                        }
                    }
                });
        scoreDocs = topDocs.toArray(new ScoreDoc[topDocs.size()]);
        return scoreDocs;
    }

    @Override
    public ScoreMode scoreMode() {
        return null;
    }
}