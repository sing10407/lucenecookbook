package Chapter3;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.SortedDocValuesField;
import org.apache.lucene.index.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.MMapDirectory;
import org.apache.lucene.util.BytesRef;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;

public class MyDocValueField {
    public static void main(String[] argv) throws IOException {
        Analyzer analyzer = new StandardAnalyzer();
        Directory directory = new MMapDirectory(new File("gg").toPath());
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        IndexWriter indexWriter = new IndexWriter(directory, config);
        Document document = new Document();

        // Add DocValues
        document.add(new SortedDocValuesField("sorted_string", new BytesRef("hello")));
        indexWriter.addDocument(document);
        document = new Document();
        document.add(new SortedDocValuesField("sorted_string", new BytesRef("world")));
        indexWriter.addDocument(document);

        indexWriter.commit();
        indexWriter.close();

        IndexReader reader = DirectoryReader.open(directory);
        document = reader.document(0);
        System.out.println("doc 0: " + document.toString());
        document = reader.document(1);
        System.out.println("doc 1: " + document.toString());

        for (LeafReaderContext context : reader.leaves()) {
            LeafReader atomicReader = context.reader();
            SortedDocValues sortedDocValues = DocValues.getSorted(atomicReader, "sorted_string");
            System.out.println("Value count: " + sortedDocValues.getValueCount());
            System.out.println("doc 0 sorted_string: " + sortedDocValues.lookupOrd(0).utf8ToString());
            System.out.println("doc 1 sorted_string: " + sortedDocValues.lookupOrd(1).utf8ToString());
        }
        reader.close();
    }
}
