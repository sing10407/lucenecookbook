package Chapter3;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.FSDirectory;

import java.io.File;
import java.io.IOException;

public class MyDocumentField {
    public static  void main(String[] argv) throws IOException {

        FSDirectory directory = FSDirectory.open(new File("gg").toPath());
        Analyzer analyzer = new StandardAnalyzer();
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        IndexWriter indexWriter = new IndexWriter(directory, config);

        Document document = new Document();

        // String Field
        document.add(
                new StringField(
                        "telephone_number", // field
                        "04735264927", // value
                        Field.Store.YES // value included in search result or not
                )
        );

        // Text Field
        String text = "Lucene is an Information Retrieval library written in Java.";
        document.add(new TextField("text", text, Field.Store.YES));

        // int field
        IntPoint intField = new IntPoint("int_value", 100);
        // if store the value (value included in search result or not)
        StoredField storedField = new StoredField("int_value", 100);

        LongPoint longField = new LongPoint("long_value", 100L);
        FloatPoint floatField = new FloatPoint("float_value", 100.0F);
        DoublePoint doubleField = new DoublePoint("double_value", 100.0D);

        document.add(intField);
        document.add(storedField);
        document.add(longField);
        document.add(floatField);
        document.add(doubleField);


        indexWriter.addDocument(document);
        indexWriter.commit();

    }
}
