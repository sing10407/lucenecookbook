package Chapter3;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.IOException;

public class MyIndexWriter {
    public static void main(String[] argv) throws IOException {
        FSDirectory directory = FSDirectory.open(new File("gg").toPath());
        Analyzer analyzer = new StandardAnalyzer();

        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        config.setOpenMode(IndexWriterConfig.OpenMode.CREATE); // Configure OpenMode here
        config.setRAMBufferSizeMB(64);
        config.setMaxBufferedDocs(4000);

        IndexWriter indexWriter = new IndexWriter(directory, config);
    }
}
