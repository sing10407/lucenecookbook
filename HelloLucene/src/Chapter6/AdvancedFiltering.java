package Chapter6;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.*;
import org.apache.lucene.index.*;
import org.apache.lucene.search.*;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.apache.lucene.util.Version;

import java.io.IOException;

public class AdvancedFiltering {
    public static void main(String[] argv) throws IOException {
        Analyzer analyzer = new StandardAnalyzer();
        Directory directory = new RAMDirectory();
        IndexWriterConfig config = new IndexWriterConfig(analyzer);
        IndexWriter indexWriter = new IndexWriter(directory, config);

        addDocuments(indexWriter);

        indexWriter.commit();
        indexWriter.close();
        IndexReader indexReader = DirectoryReader.open(directory);
        IndexSearcher indexSearcher = new IndexSearcher(indexReader);

        // test cases

        System.out.println("Basic search on word `humpty` : ");
        TestCase1_basic(indexReader, indexSearcher);

        System.out.println("search on word `humpty`, use  TermRangeQuery: ");
        TestCase2_TermRangeQuery(indexReader, indexSearcher);

        System.out.println("search without word, use IntPoint.newRangeQuery: ");
        TestCase3_NumericRangeFilter(indexReader, indexSearcher);

        System.out.println("search with prefix F, use PrefixQuery");
        TestCase4_PrefixQuery(indexReader, indexSearcher);

        System.out.println("search without word, use DocValuesFieldExistsQuery");
        TestCase5_DocValuesFieldExistsQuery(indexReader, indexSearcher);

        // FieldCacheRangeFilter is deprecated

        // QueryWrapperFilter is deprecated
        // You can use Query objects directly for filtering by using BooleanClause.Occur.FILTER clauses in a BooleanQuery.
        // http://lucene.apache.org/core/5_5_5/core/org/apache/lucene/search/QueryWrapperFilter.html

        // CachingWrapperQuery is deprecated

        indexReader.close();
    }


    private static void TestCase5_DocValuesFieldExistsQuery(IndexReader indexReader, IndexSearcher indexSearcher) throws IOException {
        // https://lucene.apache.org/core/5_4_0/core/org/apache/lucene/search/FieldValueFilter.html
        // Use FieldValueQuery instead

        // https://lucene.apache.org/core/7_4_0/MIGRATE.html
        // FieldValueQuery is renamed to DocValuesFieldExistsQuery
        DocValuesFieldExistsQuery query = new DocValuesFieldExistsQuery ("name1");

        TopDocs topDocs = indexSearcher.search(query ,10);
        printSearchResult(indexReader, topDocs);
        System.out.println("");
    }

    private static void TestCase4_PrefixQuery(IndexReader indexReader, IndexSearcher indexSearcher) throws IOException {
        PrefixQuery prefixQuery = new PrefixQuery(new Term("name", "F"));
        TopDocs topDocs = indexSearcher.search(prefixQuery ,10);
        printSearchResult(indexReader, topDocs);
        System.out.println("");
    }

    private static void TestCase3_NumericRangeFilter(IndexReader indexReader, IndexSearcher indexSearcher) throws IOException {
        // NumericRangeFilter and NumericRangeQuery is deprecated, Use NumericRangeQuery and BooleanClause.Occur.FILTER clauses instead.
        // http://lucene.apache.org/core/5_5_5/core/org/apache/lucene/search/NumericRangeFilter.html
        // https://lucene.apache.org/core/6_0_0/core/org/apache/lucene/search/LegacyNumericRangeQuery.html
        // NumericRangeQuery numericRangeQuery = NumericRangeQuery.newIntRange("num" , 200, 400, true, true);

        Query query = IntPoint.newRangeQuery("num",200,400);
        TopDocs topDocs = indexSearcher.search(query ,10);
        printSearchResult(indexReader, topDocs);
        System.out.println("");
    }

    private static void TestCase2_TermRangeQuery(IndexReader indexReader, IndexSearcher indexSearcher) throws IOException {
        // TermRangeFilter is deprecated, Use TermRangeQuery and BooleanClause.Occur.FILTER clauses instead.
        // https://lucene.apache.org/core/5_3_1/core/org/apache/lucene/search/TermRangeFilter.html
        TermRangeQuery termRangeQuery = TermRangeQuery.newStringRange("name", "A", "G", true, true);
        TopDocs topDocs = indexSearcher.search(termRangeQuery ,10);
        printSearchResult(indexReader, topDocs);
        System.out.println("");
    }

    private static void TestCase1_basic(IndexReader indexReader, IndexSearcher indexSearcher) throws IOException {
        Query query = new TermQuery(new Term("content", "humpty"));
        TopDocs topDocs = indexSearcher.search(query ,10);
        printSearchResult(indexReader, topDocs);
        System.out.println("");
    }

    private static void printSearchResult(IndexReader indexReader, TopDocs topDocs) throws IOException {
        Document doc;
        System.out.println("Searching 'humpty'");
        for (ScoreDoc scoreDoc : topDocs.scoreDocs) {
            doc = indexReader.document(scoreDoc.doc);
            System.out.println(
                    "name: " +doc.getField("name").stringValue() +
                            " - content: " +doc.getField("content").stringValue() +
                            " - num: " +doc.getField("num").stringValue()
            );
        }
    }

    private static void addDocuments(IndexWriter indexWriter) throws IOException {
        Document doc = new Document();
        StringField stringField = new StringField("name", "", Field.Store.YES);
        TextField textField = new TextField("content", "", Field.Store.YES);
        IntPoint intField = new IntPoint("num", 0);
        StoredField storedIntField = new StoredField("num", 0);

        doc.removeField("name");
        doc.removeField("content");
        doc.removeField("num");
        stringField.setStringValue("First");
        textField.setStringValue("Humpty Dumpty sat on a wall,");
        intField.setIntValue(100);
        storedIntField.setIntValue(100);
        doc.add(stringField);
        doc.add(textField);
        doc.add(intField);
        doc.add(storedIntField);
        indexWriter.addDocument(doc);

        doc.removeField("name");
        doc.removeField("content");
        doc.removeField("num");
        stringField.setStringValue("Second");
        textField.setStringValue("Humpty Dumpty had a great fall.");
        intField.setIntValue(200);
        storedIntField.setIntValue(200);
        doc.add(stringField);
        doc.add(textField);
        doc.add(intField);
        doc.add(storedIntField);
        indexWriter.addDocument(doc);

        doc.removeField("name");
        doc.removeField("content");
        doc.removeField("num");
        stringField.setStringValue("Third");
        textField.setStringValue("All the king's horses and all the king's men");
        intField.setIntValue(300);
        storedIntField.setIntValue(300);
        doc.add(stringField);
        doc.add(textField);
        doc.add(intField);
        doc.add(storedIntField);
        indexWriter.addDocument( doc);

        doc.removeField("name");
        doc.removeField("content");
        doc.removeField("num");
        stringField.setStringValue("Fourth");
        textField.setStringValue("Couldn't put Humpty together again.");
        intField.setIntValue(400);
        storedIntField.setIntValue(400);
        doc.add(stringField);
        doc.add(textField);
        doc.add(intField);
        doc.add(storedIntField);
        indexWriter.addDocument(doc);
    }
}
