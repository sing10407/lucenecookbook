package Chapter2.Custom;

import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;

import java.io.IOException;
import java.io.StringReader;

public class GenderFilter extends TokenFilter {
    GenderAttribute genderAtt = addAttribute(GenderAttribute.class);
    CharTermAttribute charTermAtt = addAttribute(CharTermAttribute.class);

    protected GenderFilter(TokenStream input) {
        super(input);
    }
    public boolean incrementToken() throws IOException {
        if (!input.incrementToken()) {
            return false;
        }
        genderAtt.setGender(determineGender(charTermAtt.toString()));
        return true;
    }
    protected GenderAttribute.Gender determineGender(String term) {
        if (term.equalsIgnoreCase("mr") || term.equalsIgnoreCase("mister")) {
            return GenderAttribute.Gender.Male; }
        else if (term.equalsIgnoreCase("mrs") || term.equalsIgnoreCase("misters")) {
            return GenderAttribute.Gender.Female;
        }
        return GenderAttribute.Gender.Undefined;
    }

    public static void main(String[] argv) throws IOException {
        StringReader reader = new StringReader("Dr kerker and Mrs GG are couple.");
        StandardAnalyzer standardAnalyzer = new StandardAnalyzer();
        TokenStream ts = null;

        try {
            ts = standardAnalyzer.tokenStream("field", reader);
            // use new token filter
            ts = new GenderFilter(ts);
            CharTermAttribute termAtt = ts.getAttribute(CharTermAttribute.class);
            GenderAttribute genderAtt = ts.getAttribute(GenderAttribute.class);

            ts.reset();
            while (ts.incrementToken()) {
                System.out.println("[" + termAtt.toString() +" Gender："+genderAtt.getGender()+"]");
            }
            ts.end();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            ts.close();
            standardAnalyzer.close();
        }

    }
}