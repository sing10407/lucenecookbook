package Chapter2.Custom;


import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;

import java.io.IOException;
import java.io.StringReader;
import java.util.HashMap;
import java.util.Map;

public class CourtesyTitleFilter extends TokenFilter {
    public static void main(String[] argv) throws IOException {
        StringReader reader = new StringReader("Dr kerker and Mrs GG are couple.");
        StandardAnalyzer standardAnalyzer = new StandardAnalyzer();
        TokenStream ts = null;

        try {
            ts = standardAnalyzer.tokenStream("field", reader);
            // use new token filter
            ts = new CourtesyTitleFilter(ts);

            CharTermAttribute termAtt = ts.addAttribute(CharTermAttribute.class);
            
            ts.reset();
            while (ts.incrementToken()) {
                System.out.println("[" + termAtt.toString() + "]");
            }
            ts.end();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            ts.close();
            standardAnalyzer.close();
        }

    }

    Map<String,String> courtesyTitleMap = new HashMap<String,String>();
    private CharTermAttribute termAttr;
    public CourtesyTitleFilter(TokenStream input) {
        super(input);
        termAttr = addAttribute(CharTermAttribute.class);
        courtesyTitleMap.put("dr", "doctor");
        courtesyTitleMap.put("mr", "mister");
        courtesyTitleMap.put("mrs", "miss");
    }
    @Override
    public boolean incrementToken() throws IOException {
        if (!input.incrementToken()) return false;
        String small = termAttr.toString();
        if(courtesyTitleMap.containsKey(small)) {
            termAttr.setEmpty().append(courtesyTitleMap.get(small));
        }
        return true;
    }
}