package Chapter2.Custom;

import org.apache.lucene.util.AttributeImpl;
import org.apache.lucene.util.AttributeReflector;

// The names of the classes are important
// because Lucene looks for the Impl suffix to locate the implementation class of an Attribute interface.
public class GenderAttributeImpl extends AttributeImpl implements GenderAttribute {
    private Gender gender = Gender.Undefined;

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Gender getGender() {
        return gender;
    }

    @Override
    public void clear() {
        gender = Gender.Undefined;
    }

    @Override
    public void reflectWith(AttributeReflector attributeReflector) {
    }

    @Override
    public void copyTo(AttributeImpl target) {
        ((GenderAttribute) target).setGender(gender);
    }
}