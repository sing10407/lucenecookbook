package Chapter2.Custom;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.LetterTokenizer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.CharTokenizer;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;


public class CourtesyTitleAnalyzer extends Analyzer {
    CharTokenizer tokenizer;
    public static void main(String[] argv) throws IOException {
        StringReader reader = new StringReader("dr kerker and mrs GG are couple.");
        CourtesyTitleAnalyzer courtesyTitleAnalyzer = new CourtesyTitleAnalyzer();
        TokenStream ts = null;

        try {
            ts = courtesyTitleAnalyzer.tokenStream("field", reader);
            CharTermAttribute termAtt = ts.addAttribute(CharTermAttribute.class);

            ts.reset();
            while (ts.incrementToken()) {
                System.out.println("[" + termAtt.toString() + "]");
            }
            ts.end();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            ts.close();
            courtesyTitleAnalyzer.close();
        }
    }

    public CourtesyTitleAnalyzer(){
        tokenizer = new LetterTokenizer();
    }

    public CourtesyTitleAnalyzer(CharTokenizer customTokenizer){
        tokenizer = customTokenizer;
    }

    @Override
    protected TokenStreamComponents createComponents(String fieldName) {
        TokenStream filter = new CourtesyTitleFilter(tokenizer);
        return new TokenStreamComponents(tokenizer, filter);
    }
}