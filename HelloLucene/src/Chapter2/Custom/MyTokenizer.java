package Chapter2.Custom;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.util.CharTokenizer;
import org.apache.lucene.util.AttributeFactory;

import java.io.IOException;
import java.io.StringReader;

public class MyTokenizer extends CharTokenizer {
    public static void main(String[] argv) throws IOException {
        StringReader reader = new StringReader("test a b c gg-5566.");
        // custom tokenizer
        MyTokenizer tokenizer = new MyTokenizer();
        // inject tokenizer into a custom analyzer
        CourtesyTitleAnalyzer courtesyTitleAnalyzer = new CourtesyTitleAnalyzer(tokenizer);
        TokenStream ts = null;

        try {
            ts = courtesyTitleAnalyzer.tokenStream("field", reader);
            CharTermAttribute termAtt = ts.addAttribute(CharTermAttribute.class);

            ts.reset();
            while (ts.incrementToken()) {
                System.out.println("[" + termAtt.toString() + "]");
            }
            ts.end();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            ts.close();
            courtesyTitleAnalyzer.close();
        }
    }

    public MyTokenizer() {

        super();
    }

    public MyTokenizer(AttributeFactory factory) {

        super(factory);
    }

    @Override
    protected boolean isTokenChar(int c) {

        return !Character.isSpaceChar(c);
    }
}