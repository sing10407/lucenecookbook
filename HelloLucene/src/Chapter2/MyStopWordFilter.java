package Chapter2;

import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.core.StopAnalyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.tokenattributes.OffsetAttribute;
import org.apache.lucene.analysis.tokenattributes.PositionIncrementAttribute;

import java.io.IOException;
import java.io.StringReader;

import static org.apache.lucene.analysis.core.StopAnalyzer.*;
import static org.apache.lucene.analysis.en.EnglishAnalyzer.ENGLISH_STOP_WORDS_SET;

public class MyStopWordFilter extends TokenFilter {
    public static void main(String[] argv) throws IOException {
        StringReader reader = new StringReader("Lucene is mainly used for information retrieval and you can read more about it at lucene.apache.org.");
        StandardAnalyzer wa = new StandardAnalyzer();
        TokenStream ts = null;

        try {
            ts = wa.tokenStream("field", reader);

            ts =  new MyStopWordFilter(ts);  // filter stop words

            CharTermAttribute termAtt = ts.addAttribute(CharTermAttribute.class);
            PositionIncrementAttribute positionIncriAtt = ts.addAttribute(PositionIncrementAttribute.class);

            // reset TokenStream to the beginning.
            ts.reset();
            while (ts.incrementToken()) {
                String token = termAtt.toString();

                System.out.println("[" + token + "]");
                // greater 1 implies the previous token and current token are not consecutive
                System.out.println(" Token PositionIncrement offset: " + positionIncriAtt.getPositionIncrement());
                System.out.println("");
            }
            ts.end();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            ts.close();
            wa.close();
        }
    }


    private CharTermAttribute charTermAtt;
    public PositionIncrementAttribute posIncrAtt;

    public MyStopWordFilter(TokenStream input) {
        super(input);
        charTermAtt = addAttribute(CharTermAttribute.class);
        posIncrAtt = addAttribute(PositionIncrementAttribute.class);
    }

    @Override
    public boolean incrementToken() throws IOException {
        int extraIncrement = 0;
        boolean returnValue = false;
        while (input.incrementToken()) {
            if (ENGLISH_STOP_WORDS_SET.contains(charTermAtt.toString())) {
                extraIncrement++;// filter this word
                continue;
            }
            returnValue = true;
            break;
        }
        // have stop words
        if (extraIncrement > 0) {
            posIncrAtt.setPositionIncrement(posIncrAtt.getPositionIncrement() + extraIncrement);
        }
        return returnValue;
    }
}